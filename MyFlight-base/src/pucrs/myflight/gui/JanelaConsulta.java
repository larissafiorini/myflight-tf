package pucrs.myflight.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import pucrs.myflight.modelo.Aeroporto;
import pucrs.myflight.modelo.CiaAerea;
import pucrs.myflight.modelo.Geo;
import pucrs.myflight.modelo.GerenciadorAeroportos;
import pucrs.myflight.modelo.GerenciadorCias;
import pucrs.myflight.modelo.GerenciadorRotas;
import pucrs.myflight.modelo.Rota;

public class JanelaConsulta extends javax.swing.JFrame {

	private GerenciadorAeroportos gerAero;
	private GerenciadorRotas gerRotas;
	private GerenciadorCias gerCias;

	private ArrayList<String> codCias;

	private GerenciadorMapa gerenciador;
	private EventosMouse mouse;

	private JPanel painelMapa;
	private JPanel painelLateral;
	private JTextField txtDistancia;

	private String cia2;

	/**
	 * Creates new form JanelaConsulta
	 */
	public JanelaConsulta() {
		super();
		// initComponents();

		GeoPosition poa = new GeoPosition(-30.05, -51.18);
		gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
		mouse = new EventosMouse();
		gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
		gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);

		painelMapa = new JPanel();
		painelMapa.setLayout(new BorderLayout());
		painelMapa.add(gerenciador.getMapKit(), BorderLayout.CENTER);

		getContentPane().add(painelMapa, BorderLayout.CENTER);

		painelLateral = new JPanel();
		painelLateral.setLayout(new BoxLayout(painelLateral, BoxLayout.Y_AXIS));
		getContentPane().add(painelLateral, BorderLayout.WEST);

		inicializaComponentes();

		this.setSize(800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("MyFlight");
	}

	public void inicializaComponentes() {

		String[] cias = new String[] { "2B", "2G", "2I", "2J", "2K", "2L", "2N", "2O", "2P", "2Z", "3E", "3F", "3G",
				"3H", "3J", "3K", "3L", "3M", "3O", "3P", "3R", "3S", "3U", "4B", "4D", "4E", "4G", "4H", "4K", "4M",
				"4N", "4O", "4Q", "4T", "4U", "4W", "4Y", "5C", "5G", "5H", "5J", "5M", "5N", "5P", "5Q", "5T", "5U",
				"5Z", "6E", "6H", "6I", "6L", "6R", "6T", "6W", "6Y", "7C", "7E", "7F", "7G", "7H", "7I", "7J", "7M",
				"7P", "7R", "7S", "8B", "8D", "8E", "8I", "8L", "8M", "8P", "8Q", "8R", "8T", "8U", "8V", "9C", "9D",
				"9E", "9K", "9M", "9N", "9Q", "9R", "9U", "9V", "9W", "A2", "A3", "A4", "A5", "A8", "A9", "AA", "AB",
				"ABJ", "AC", "AD", "AE", "AF", "AH", "AI", "AK", "AM", "AP", "AR", "AS", "AT", "AV", "AW", "AY", "AZ",
				"B2", "B5", "B6", "B7", "B9", "BA", "BB", "BC", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM",
				"BP", "BR", "BT", "BU", "BV", "BW", "BX", "C8", "CA", "CC", "CE", "CG", "CI", "CM", "CN", "CU", "CX",
				"CY", "CZ", "D2", "D3", "D6", "D7", "DC", "DD", "DE", "DG", "DH", "DL", "DN", "DT", "DV", "DX", "DY",
				"DZ", "E2", "E5", "E7", "E8", "E9", "EB", "EF", "EG", "EI", "EK", "EL", "EN", "EO", "EP", "EQ", "ET",
				"EU", "EW", "EY", "F2", "F5", "F7", "F9", "FB", "FD", "FE", "FG", "FI", "FJ", "FL", "FM", "FO", "FR",
				"FS", "FV", "FW", "FY", "FZ", "G3", "G4", "G5", "G8", "G9", "GA", "GE", "GF", "GK", "GL", "GQ", "GR",
				"GS", "GV", "GY", "GZ", "H2", "H7", "HA", "HD", "HF", "HG", "HI", "HK", "HM", "HN", "HO", "HQ", "HR",
				"HS", "HU", "HV", "HW", "HX", "HY", "HZ", "I4", "I6", "I8", "I9", "IB", "ID", "IE", "IG", "IL", "IOS",
				"IR", "IX", "IY", "IZ", "J2", "J3", "J4", "J5", "J8", "J9", "JA", "JB", "JD", "JE", "JH", "JJ", "JL",
				"JN", "JP", "JQ", "JS", "JT", "JU", "JV", "JW", "JY", "K2", "K3", "K5", "K6", "K8", "KA", "JP", "JQ",
				"JS", "JT", "JU", "JV", "JW", "JY", "K2", "K3", "K5", "K6", "K8", "KB", "KC", "KE", "KK", "KL", "KM",
				"KN", "KO", "KQ", "KR", "KS", "KU", "KX", "KY", "L6", "LA", "LC", "LF", "LG", "LH", "LI", "LJ", "LM",
				"LN", "LO", "LR", "LS", "LT", "LV", "LW", "LX", "LY", "M3", "M4", "M5", "M6", "M9", "MD", "ME", "MF",
				"MH", "MI", "MJ", "MK", "MM", "MN", "MO", "MR", "MS", "MU", "MW", "MY", "N3", "NF", "NH", "NHG", "NK",
				"NL", "NP", "NS", "NT", "NU", "NX", "NY", "NZ", "O4", "O6", "OA", "OB", "OC", "OD", "OJ", "OK", "OM",
				"ON", "OR", "OS", "OU", "OV", "OX", "OY", "OZ", "P0", "P1", "P2", "P4", "P6", "P9", "PB", "PC", "PD",
				"PE", "PG", "PI", "PJ", "PK", "PL", "PM", "PN", "PQ", "PR", "PS", "PV", "PW", "PX", "PY", "PZ", "Q2",
				"Q3", "Q5", "Q6", "Q7", "Q8", "Q9", "QB", "QC", "QF", "QG", "QH", "QL", "QN", "QR", "QS", "QU", "QV",
				"QZ", "R2", "R3", "R7", "RA", "RC", "RG", "RI", "RJ", "RL", "RO", "RQ", "RX", "RZ", "S2", "S3", "S4",
				"S5", "S6", "S7", "S9", "SA", "SB", "SC", "SD", "SE", "SG", "SH", "SI", "SJ", "SK", "SN", "SP", "SQ",
				"SS", "ST", "SU", "SV", "SW", "SX", "SY", "SZ", "T3", "T5", "T7", "TA", "TCX", "TD", "TE", "TF", "TG",
				"TJ", "TK", "TL", "TM", "TN", "TO", "TOM", "TP", "TR", "TS", "TT", "TU", "TV", "TW", "TX", "TY", "TZ",
				"U2", "U6", "U7", "UA", "UD", "UE", "UG", "UJ", "UL", "UM", "UN", "UO", "UP", "UR", "US", "UU", "UX",
				"UY", "V0", "V3", "V4", "V7", "V9", "VA", "VB", "VF", "VH", "VJ", "VN", "VQ", "VR", "VS", "VT", "VW",
				"VX", "VY", "W2", "W3", "W4", "W5", "W6", "W9", "WB", "WF", "WJ", "WM", "WN", "WP", "WS", "WT", "WU",
				"WW", "WX", "WY", "X3", "X4", "X7", "XK", "XL", "XQ", "XU", "XV", "XY", "Y4", "Y5", "Y7", "Y8", "Y9",
				"YC", "YI", "YJ", "YK", "YM", "YN", "YO", "YQ", "YR", "YT", "Z2", "Z3", "Z4", "Z6", "Z8", "Z9", "ZB",
				"ZD", "ZE", "ZH", "ZI", "ZK", "ZL", "ZM"};

		final JComboBox<String> jbCias = new JComboBox<String>(cias);

		Label labelDistancia = new Label("Dist�ncia:");
		txtDistancia = new JTextField();


		JButton btnConsulta1 = new JButton("Consulta 1");
		btnConsulta1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consulta(e);
			}
		});

		JButton btnConsulta2 = new JButton("Consulta 2");
		btnConsulta2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consulta2(e,txtDistancia.getText().toString());
			}
		});

		JButton btnConsulta3 = new JButton("Consulta 3");
		btnConsulta3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consulta3(e);
			}
		});


		JButton btnConsulta4 = new JButton("Consulta 4");
		btnConsulta4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selected = (String) jbCias.getSelectedItem();
				System.out.println("Você selecionou a cia: " + selected);
				cia2 = selected;
				consulta4(e, cia2);
			}
		});

		painelLateral.add(btnConsulta1);
		painelLateral.add(labelDistancia);
		painelLateral.add(txtDistancia);
		painelLateral.add(btnConsulta2);
		painelLateral.add(btnConsulta3);
		painelLateral.add(jbCias);

		painelLateral.add(btnConsulta4);

	}

	public void setGerAeroportos(GerenciadorAeroportos ger) {
		this.gerAero = ger;
	}

	public void setGerRotas(GerenciadorRotas ger) {
		this.gerRotas = ger;
	}

	public void setGerCias(GerenciadorCias ger) {
		this.gerCias = ger;
	}

	public void setCodCias(ArrayList<String> acc) {
		this.codCias = acc;
	}

	private void consulta(ActionEvent evt) {

		gerenciador.clear();

		// Para obter um ponto clicado no mapa, usar como segue:
		GeoPosition pos = gerenciador.getPosicao();
		String paisatual="";

		double raio=2;
		int conta = gerAero.quantidade();
		for (int i = 0; i < conta; i++) {
			Aeroporto p = gerAero.listarTodos().get(i);
			Geo g = p.getLocal();

			if ((g.getLatitude() >= pos.getLatitude()) && (g.getLatitude() <= (pos.getLatitude() + raio))) {
				if ((g.getLongitude() >= pos.getLongitude()) && (g.getLongitude() <= (pos.getLongitude() + raio))) {
					System.out.println("Cód. aero: " + p.getCodigo() + g.getLatitude() + g.getLongitude());
					paisatual =p.getCodpais();
					break;
				}
			}
		}

		// Lista para armazenar o resultado da consulta
		List<MyWaypoint> lstPoints = new ArrayList<>();

		int cont = gerAero.quantidade();
		for (int i = 0; i < cont; i++) {

			Aeroporto p = gerAero.listarTodos().get(i);
			Geo g = p.getLocal();

			if (p.getCodpais().equals(paisatual)) {
				lstPoints.add(new MyWaypoint(Color.BLUE, p.getCodigo(), g));
			}

		}

		// Informa o resultado para o gerenciador
		gerenciador.setPontos(lstPoints);

		this.repaint();

	}

	private void consulta2(ActionEvent evt, String dista) {

		gerenciador.clear();

		GeoPosition pos = gerenciador.getPosicao();
		String codaero="";
		//System.out.println("POS= "+pos.getLatitude()+pos.getLongitude());
		double raio=1;
		int conta = gerAero.quantidade();
		for (int i = 0; i < conta; i++) {
			Aeroporto p = gerAero.listarTodos().get(i);
			Geo g = p.getLocal();

			if ((g.getLatitude() >= pos.getLatitude()) && (g.getLatitude() <= (pos.getLatitude() + raio))) {
				if ((g.getLongitude() >= pos.getLongitude()) && (g.getLongitude() <= (pos.getLongitude() + raio))) {
					System.out.println("Cód. aero: " + p.getCodigo() + g.getLatitude() + g.getLongitude());
					codaero =p.getCodigo();
					break;
				}
			}
		}

		List<MyWaypoint> lstPoints = new ArrayList<>();

		Aeroporto aeroporto = gerAero.buscarCodigo(codaero);

		for (int i = 0; i < gerRotas.listarTodas().size(); i++) {
			String origin = gerRotas.listarTodas().get(i).getOrig();
			if(aeroporto != null) {
				if (aeroporto.getCodigo() != null && origin != null && aeroporto.getCodigo().equals(origin)) {
					String destino = gerRotas.listarTodas().get(i).getDest();
					if(destino != null) {
						Aeroporto aeroportoDestino = gerAero.buscarCodigo(destino);
						Tracado tracado = new Tracado();

						Geo geoAeroporto = aeroporto.getLocal();
						Geo geoDestino = aeroportoDestino.getLocal();
						double distancia = geoAeroporto.distancia(geoDestino);
						if(Double.parseDouble(dista) >= distancia) {

							lstPoints.add(new MyWaypoint(Color.BLACK, aeroporto.getNome(), geoAeroporto));
							lstPoints.add(new MyWaypoint(Color.BLACK, aeroportoDestino.getNome(), geoDestino));

							tracado.addPonto(geoAeroporto);
							tracado.addPonto(geoDestino);
							tracado.setCor(Color.RED);
							gerenciador.setPontos(lstPoints);
							gerenciador.addTracado(tracado);
						}
					}
				}
			} else {
				System.out.println("Este aeroporto n�o possui destinos");
			}
		}
		this.repaint();
	}

	private void consulta3(ActionEvent evt) {

		gerenciador.clear();

		GeoPosition pos = gerenciador.getPosicao();
		String codaero="";
		//System.out.println("POS= "+pos.getLatitude()+pos.getLongitude());
		double raio=1;
		int conta = gerAero.quantidade();
		for (int i = 0; i < conta; i++) {
			Aeroporto p = gerAero.listarTodos().get(i);
			Geo g = p.getLocal();

			if ((g.getLatitude() >= pos.getLatitude()) && (g.getLatitude() <= (pos.getLatitude() + raio))) {
				if ((g.getLongitude() >= pos.getLongitude()) && (g.getLongitude() <= (pos.getLongitude() + raio))) {
					System.out.println("Cód. aero: " + p.getCodigo() + g.getLatitude() + g.getLongitude());
					codaero =p.getCodigo();
					break;
				}
			}
		}
		List<MyWaypoint> lstPoints = new ArrayList<>();

		lstPoints.clear();

		Aeroporto aeroporto = gerAero.buscarCodigo(codaero);

		int cont = 3;
		int contGeral = gerRotas.listarTodas().size();

		while (contGeral > 0) {

			while (cont > 0) {
				for (int i = 0; i < gerRotas.listarTodas().size(); i++) {
					String origin = gerRotas.listarTodas().get(i).getOrig();
					// PEGA AEROPORTO Q COME�A A ROTA
					if (aeroporto.getCodigo().equals(origin)) {
						// DESENHA PRIMEIRA ROTA
						String destt = gerRotas.listarTodas().get(i).getDest();
						Aeroporto dest = gerAero.buscarCodigo(destt);
						Tracado tr = new Tracado();
						Geo go = aeroporto.getLocal();
						Geo gd = dest.getLocal();
						lstPoints.add(new MyWaypoint(Color.GREEN, aeroporto.getNome(), go));
						lstPoints.add(new MyWaypoint(Color.BLACK, dest.getNome(), gd));

						tr.addPonto(go);
						tr.addPonto(gd);
						tr.setCor(Color.RED);
						gerenciador.setPontos(lstPoints);
						gerenciador.addTracado(tr);

						aeroporto = dest;
						break;
					}
				}
				cont--;
			}
			contGeral--;
			aeroporto = gerAero.buscarCodigo("SYD");
			this.repaint();
		}
		this.repaint();

	}

	private void consulta4(ActionEvent evt, String compa) {

		gerenciador.clear();

		List<MyWaypoint> lstPoints = new ArrayList<>();

		String cia = gerRotas.listarTodas().get(0).getCiaa();

		System.out.println("CIA=" + cia);

		CiaAerea ca = gerCias.buscarCodigo(cia);

		System.out.println(ca.getCodigo());

		System.out.println("CIA SELECIONADA: " + compa);

		String cia_oficial = compa;

		for (int i = 0; i < gerRotas.listarTodas().size(); i++) {
			String orig = gerRotas.listarTodas().get(i).getOrig();
			String dest = gerRotas.listarTodas().get(i).getDest();

			String ciaAerea = gerRotas.listarTodas().get(i).getCiaa();
			if (ciaAerea.equals(cia_oficial)) {
				Aeroporto aeroportoOrigem = gerAero.buscarCodigo(orig);
				Aeroporto aeroportoDestino = gerAero.buscarCodigo(dest);

				Tracado tracado = new Tracado();
				Geo geoOrigem = aeroportoOrigem.getLocal();
				Geo geoDestino = aeroportoDestino.getLocal();

				lstPoints.add(new MyWaypoint(Color.BLACK, aeroportoOrigem.getNome(), geoOrigem));
				lstPoints.add(new MyWaypoint(Color.BLACK, aeroportoDestino.getNome(), geoDestino));

				tracado.addPonto(geoOrigem);
				tracado.addPonto(geoDestino);
				tracado.setCor(Color.RED);

				// Informa o resultado para o gerenciador
				gerenciador.setPontos(lstPoints);
				gerenciador.addTracado(tracado);
			}
		}
		this.repaint();

	}



	private void mouseClique() {
		GeoPosition geoMousePosicao = gerenciador.getPosicao();
		System.out.println(geoMousePosicao.getLatitude() + ", " + geoMousePosicao.getLongitude());

		// -29.993002284551064, -51.170196533203125 Local. aproximada clicada na
		// tela
		// -29.9939;-51.1711 Posi��o exata do aerop. Salgado Filho
		double valor = geoMousePosicao.getLatitude();
		double valorExiste = -29.9939;

	}

	private class EventosMouse extends MouseAdapter {
		private int lastButton = -1;

		@Override
		public void mousePressed(MouseEvent e) {
			JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
			GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
			System.out.println(loc.getLatitude() + ", " + loc.getLongitude());
			lastButton = e.getButton();
			// Botão 3: seleciona localização
			if (lastButton == MouseEvent.BUTTON3) {
				gerenciador.setPosicao(loc);
				// gerenciador.getMapKit().setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
				gerenciador.getMapKit().repaint();

			}
			gerenciador.setPosicao(loc);
			gerenciador.getMapKit().repaint();
			mouseClique();
		}
	}

}