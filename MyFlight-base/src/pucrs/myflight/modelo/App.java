package pucrs.myflight.modelo;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

import pucrs.myflight.gui.JanelaConsulta;

public class App {

	public static void main(String[] args) {
		CiaAerea gol = new CiaAerea("G3", "Gol Linhas A�reas SA");
		CiaAerea latam = new CiaAerea("JJ", "LATAM Linhas A�reas");
	

		Aeroporto poa = new Aeroporto("POA", "Salgado Filho Intl Apt", new Geo(-29.9939, -51.1711), "BR");
		Aeroporto gru = new Aeroporto("GRU", "São Paulo Guarulhos Intl Apt", new Geo(-23.4356, -46.4731), "BR");
		

		double dist = Geo.distancia(poa.getLocal(), gru.getLocal());
		

		double dist2 = poa.getLocal().distancia(gru.getLocal());

		GerenciadorAeroportos gerAero = new GerenciadorAeroportos();
		gerAero.adicionar(poa);
		gerAero.adicionar(gru);

		GerenciadorAeronaves gerAvioes = new GerenciadorAeronaves();
		gerAvioes.adicionar(new Aeronave("73G", "Boeing 737-700", 126));
		gerAvioes.adicionar(new Aeronave("733", "Boeing 737-300", 140));
		gerAvioes.adicionar(new Aeronave("380", "Airbus Industrie A380", 644));
		gerAvioes.ordenarDescricao();

		Aeronave a1 = gerAvioes.buscarCodigo("733");
		Aeronave a2 = gerAvioes.buscarCodigo("73G");

		
		System.out.println();
		
		LocalDateTime datahora1 = LocalDateTime.of(2016, 8, 18, 8, 30);
		Duration duracao1 = Duration.ofMinutes(90);

		
		GerenciadorCias gerCias = new GerenciadorCias();
	
		try {
			gerCias.carregaDados();
		} catch (IOException e) {
			System.out.println("Imposs�vel ler airlines.dat!");
			System.out.println("Msg: " + e);
			System.exit(1);
		}
		
		try {
			gerAero.carregaDados();
		} catch (IOException e) {
			System.out.println("Imposs�vel ler airports.dat!");
			System.out.println("Msg: " + e);
			System.exit(1);
		}
		GerenciadorRotas gerRotas = new GerenciadorRotas();
		try {
			gerAvioes.carregaDados();
		} catch (IOException e) {
			System.out.println("Imposs�vel ler equipment.dat!");
			System.out.println("Msg: " + e);
			System.exit(1);
		}
		GerenciadorPaises gerPa = new GerenciadorPaises();
		try {
			gerPa.carregaDados();
		} catch (IOException e) {
			System.out.println("Imposs�vel ler countries.dat!");
			System.out.println("Msg: " + e);
			System.exit(1);
		}
		try {
			gerRotas.carregaDados();
		} catch (IOException e) {
			System.out.println("Imposs�vel ler routes.dat!");
			System.out.println("Msg: " + e);
			System.exit(1);
		}		

		// Teste GUI: abre janela
		JanelaConsulta janela = new JanelaConsulta();

		janela.setGerAeroportos(gerAero);
		janela.setGerCias(gerCias);
		janela.setGerRotas(gerRotas);
	
		janela.setVisible(true);

	}
}
