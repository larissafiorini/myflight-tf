package pucrs.myflight.modelo;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;



public class GerenciadorAeroportos {

	private ArrayList<Aeroporto> aeroportos;

	public GerenciadorAeroportos() {
		aeroportos = new ArrayList<>();
	}

	public void adicionar(Aeroporto a) {
		aeroportos.add(a);
	}

	public void carregaDados() throws IOException {
		Path path2 = Paths.get("airports.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.forName("UTF-8"))) {
			String linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			while ((linha = br.readLine()) != null) {
				Scanner sc = new Scanner(linha).useDelimiter(";"); // separador
																	// � ;
				String codigo, nome, pais;
				Double g1, g2;
				codigo = sc.next();
				g1 = Double.parseDouble(sc.next());
				g2 = Double.parseDouble(sc.next());
				nome = sc.next();
				pais = sc.next();
				Geo loc = new Geo(g1, g2);
				GerenciadorPaises gerPa = new GerenciadorPaises();
				Pais p = gerPa.buscarCodigo(pais);
				Aeroporto nova = new Aeroporto(codigo, nome, loc, pais);
				aeroportos.add(nova);

			//	System.out.println(codigo + "," + nome + "," + g1 + "," + g2 + ",PA�S==" + p);
			}
		}
		System.out.println("Total aeroportos: " + aeroportos.size());
	}

	public ArrayList<Aeroporto> listarTodos() {
		return new ArrayList<Aeroporto>(aeroportos);
	}

	public int quantidade() {
		int cont = 0;
		for (Aeroporto a : aeroportos) {
			cont++;
		}
		return cont;
	}

	public Aeroporto buscarCodigo(String codigo) {
		for (Aeroporto a : aeroportos) {
			if (codigo.equals(a.getCodigo()))
				return a;
		}
		return null; // não achamos!
	}
	public Aeroporto buscarLocal(Double lat, Double longit) {
		for (Aeroporto a : aeroportos) {
			if ((lat == a.getLocal().getLatitude()) && (longit == a.getLocal().getLongitude()))
				return a;
		}
		return null; // não achamos!
	}
	
}
