package pucrs.myflight.modelo;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;



public class GerenciadorPaises {
	private ArrayList<Pais> paises;

	public GerenciadorPaises() {
		paises = new ArrayList<>();
	}

	public void adicionar(Pais p) {
		paises.add(p);
	}

	public void carregaDados() throws IOException {
		Path path2 = Paths.get("countries.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.defaultCharset())) {
			String linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			while ((linha = br.readLine()) != null) {
				Scanner sc = new Scanner(linha).useDelimiter(";"); // separador
																	// � ;
				String codigo, nome;
				codigo = sc.next();
				nome = sc.next();

				Pais nova = new Pais(codigo, nome);
				paises.add(nova);
			}
		}
		System.out.println("Total pa�ses: " + paises.size());
	}

	public ArrayList<Pais> listarTodos() {
		return new ArrayList<Pais>(paises);
	}

	public Pais buscarCodigo(String codigo) {
		for (Pais pa : paises) {
			if (codigo.equals(pa.getCodigo()))
				return pa;
		}
		return null; // não achamos!
	}

}
