package pucrs.myflight.modelo;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;



public class GerenciadorRotas {

	private ArrayList<Rota> rotas;

	public GerenciadorRotas() {
		rotas = new ArrayList<>();
	}

	public void adicionar(Rota r) {
		rotas.add(r);
	}

	public void ordenarCia() {
		rotas.sort((Rota r1, Rota r2) -> r1.getCia().getNome().compareTo(r2.getCia().getNome()));
	}

	public void ordenarOrigem() {
		rotas.sort((Rota r1, Rota r2) -> r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome()));
	}

	public void ordenarOrigemCia() {
		rotas.sort((Rota r1, Rota r2) -> {
			int res = r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome());
			if (res != 0)
				return res;
			// Desempata pelo nome da cia.
			return r1.getCia().getNome().compareTo(r2.getCia().getNome());
		});
		// ou:
		rotas.sort(Comparator.comparing((Rota r) -> r.getOrigem().getNome()).thenComparing(r -> r.getCia().getNome()));
	}

	public void carregaDados() throws IOException {
		Path path2 = Paths.get("routes.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.defaultCharset())) {
			String linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			linha = br.readLine();
			System.out.println("Cabe�alho: " + linha);
			while ((linha = br.readLine()) != null) {
				Scanner sc = new Scanner(linha).useDelimiter(";"); // separador
																	// � ;
				String aeronave, aeroporto_or, aeroporto_dest, n1, n2, cia;
				cia = sc.next();
				aeroporto_or = sc.next();
				aeroporto_dest = sc.next();
				n1 = sc.next();
				n2 = sc.next();
				aeronave = sc.next();
/*
				GerenciadorAeronaves gerAvioes2 = new GerenciadorAeronaves();
				Aeronave a1 = gerAvioes2.buscarCodigo(aeronave);
				GerenciadorAeroportos gerAerop = new GerenciadorAeroportos();
				Aeroporto ap1 = gerAerop.buscarCodigo(aeroporto_or);
				Aeroporto ap2 = gerAerop.buscarCodigo(aeroporto_dest);

				GerenciadorCias gerCias2 = new GerenciadorCias();
				CiaAerea c1 = gerCias2.buscarCodigo(cia);
				*/
				GerenciadorAeronaves gerAvioes = new GerenciadorAeronaves();
				Aeronave a1 = gerAvioes.buscarCodigo(aeronave);
				Rota nova = new Rota(cia, aeroporto_or, aeroporto_dest, a1);
				rotas.add(nova);
				// System.out.println(aeronave+","+ aeroporto_or+","
				// +aeroporto_dest+"," +n1+"," +n2+","+ cia);
			}
		}
		// System.out.println("Total rotas: " + rotas.size());
	}

	public ArrayList<Rota> listarTodas() {
		return new ArrayList<Rota>(rotas);
	}

	public ArrayList<Rota> buscarOrigem(Aeroporto origem) {
		ArrayList<Rota> lista = new ArrayList<>();
		for (Rota r : rotas) {
			if (origem.getCodigo().equals(r.getOrigem().getCodigo()))
				lista.add(r);
		}
		return lista;
	}

	public int quantidade() {
		int cont = 0;
		for (Rota r : rotas) {
			cont++;
		}
		return cont;
	}

}
